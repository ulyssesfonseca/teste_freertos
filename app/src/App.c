/*
 * App.c
 *
 *  Created on: 26 de jan de 2017
 *      Author: Ulysses
 */


/*	CONVENSAO DO CODIGO
 *
 *	//! - INDICA QUE É UM TESTE E NÃO DEVE PERMANECER
 *	//!!- INDICA QUE HÁ UMA LOGICA NÃO IMPLEMENTADA
 *
 */

//https://community.nxp.com/thread/423722
//https://www.utwente.nl/en/eemcs/dacs/assignments/completed/bachelor/reports/B-assignment_vanderPloeg.pdf
//https://android.googlesource.com/kernel/lk/+/1d0df6996457273367e6d9d9d08bf6adb0fc9b65/lib/lwip/doc/rawapi.txt
//http://lwip.100.n7.nabble.com/bug-20237-pbuf-memory-corruption-tt14505.html
//https://lists.gnu.org/archive/html/lwip-users/2015-02/msg00060.html

#include "App.h"

#include "board.h"



static void task_MainGUI( void *pvParameters );
static void task_CTRLSys( void *pvParameters );
static void task_USB( void *pvParameters );
static void task_Led01( void *pvParameters );
static void task_IOT(void *pvParameters);
static void task_Ms( void *pvParameters );

xTaskHandle xtask_IOT = NULL;

volatile uint16_t _c_fps_gui=0;
volatile uint16_t _c_fps_CtrlSys=0;
volatile uint16_t _c_fps_Led=0;
volatile uint16_t _c_fps_USB=0;

volatile uint16_t cont_GUI=0;
volatile uint16_t cont_LED01=0;
volatile uint16_t cont_CTRLSys=0;
volatile uint16_t cont_USB=0;
volatile uint16_t cont_Touch=0;
volatile uint8_t _imprimeBarra=0;
volatile uint8_t _erro_anterior=0;
volatile uint8_t _aguarda_confirma=0;
volatile uint8_t _last_bot=0;
volatile uint8_t _last_grupos=0;
volatile uint8_t _last_ST = 0;

volatile uint16_t _c_timeOutStatus = 0;
volatile uint16_t _c_lasttimeOutStatus = 0;

volatile uint8_t _TMP_VALOR = 0;

volatile uint32_t size_Tux_png = 0;

//static __attribute__ ((used,section(".noinit.$SDRAM32"))) uint8_t heap_sram_lower[40*1024];
//static __attribute__ ((used,section(".bss.$RamPeriph32"))) uint8_t heap_sram_lower[16*1024];

//static __attribute__ ((used,section(".TEMP_APP"))) uint8_t heap_sram_lower[100*1024];
//static __attribute__ ((used,section(".bss.$RamLoc64"))) uint8_t heap_sram_upper[40*1024];
//static __attribute__ ((used,section(".bss.$RamLoc64_32"))) uint8_t heap_sram_lower[56*1024];
//static __attribute__ ((used,section(".bss.$RamPeriph32"))) uint8_t heap_sram_lower[4*1024];

//static __attribute__ ((used,section(".noinit.$SDRAM32"))) uint8_t heap_sram_lower[40*1024];

static __attribute__ ((used,section(".noinit.$RamLoc64"))) uint8_t heap_sram_lower[40*1024];

//static __attribute__ ((used,section(".noinit.$RamPeriph32"))) uint8_t heap_sram_lower[20*1024];

static HeapRegion_t xHeapRegions[] =
{
    { &heap_sram_lower[0], sizeof(heap_sram_lower)},
//	{ &heap_sram_upper[0], sizeof(heap_sram_upper)},
    {
        NULL, 0 // << Terminates the array.
    }
};





void vApplicationTickHook( void ) { }
void vApplicationIdleHook( void ){}
void vApplicationMallocFailedHook( void ) {}
//void vConfigureTimerForRunTimeStats( void ) { }
void vApplicationStackOverflowHook( TaskHandle_t pxTask, char *pcTaskName )
{
	/* This function will get called if a task overflows its stack. */

	( void ) pxTask;
	( void ) pcTaskName;

	for( ;; );
}


static void delay(uint32_t i)
{
	while (i--) {}
}
static void delayMs(uint32_t ms)
{
	delay(ms * 40000);
}




uint8_t ucMACAddress[6] = {0x00, 0x1A, 0xF1, 0x01, 0x84, 0x0E};
uint8_t ucIPAddress[ 4 ] = 			{ 192, 168, 1, 10 };
uint8_t ucNetMask[ 4 ] = 			{ 255, 255, 255, 0 };
uint8_t ucGatewayAddress[ 4 ] = 	{ 192, 168, 1, 1 };
uint8_t ucDNSServerAddress[ 4 ] = 	{ 192, 168, 1, 1 };
uint8_t ADDR[4];

#define CHECK_MAX(a,b) (a>b?1:0)

void setup_IP_Static(char *pBuf){
    int i=0, j=0, k=0;
    char buf[4];
    uint16_t value = 0;

	for(i=0; i<strlen(pBuf); i++){
        if(pBuf[i]=='.'){
                j=0;
                value = atoi(buf);
                if(CHECK_MAX(value, 255)){
                	ADDR[k] = 255;
                }
                else{
                    ADDR[k] = value;
                    k++;
                }
        }
        else{buf[j] = pBuf[i]; j++; buf[j]=0x00;}
	}

	value = atoi(buf);
    if(CHECK_MAX(value, 255)){
    	ADDR[k] = 255;
    }
    else{
        ADDR[k] = value;
        k++;
    }
}
void vApplicationIPNetworkEventHook( eIPCallbackEvent_t eNetworkEvent ){
	uint32_t ulIPAddress, ulNetMask, ulGatewayAddress, ulDNSServerAddress;
	static BaseType_t xTasksAlreadyCreated = pdFALSE;

	DEBUGOUT("vApplicationIPNetworkEventHook\n");
    /* Both eNetworkUp and eNetworkDown events can be processed here. */
	if( eNetworkEvent == eNetworkUp ){
		/* Create the tasks that use the TCP/IP stack if they have not already
        been created. */

		/*
		 * For convenience, tasks that use FreeRTOS+TCP can be created here
		 * to ensure they are not created before the network is usable.
		 */

		DEBUGOUT("Conexão estabelecida\n");

		FreeRTOS_GetAddressConfiguration( &ulIPAddress,
				&ulNetMask,
				&ulGatewayAddress,
				&ulDNSServerAddress );
		FreeRTOS_inet_ntoa( ulIPAddress, IP_used );
		DEBUGOUT( "IP Address: %s\r\n", IP_used );

		/* Convert the net mask to a string then print it out. */
		FreeRTOS_inet_ntoa( ulNetMask, MASK_used );
		DEBUGOUT( "Subnet Mask: %s\r\n", MASK_used );

		/* Convert the IP address of the gateway to a string then print it out. */
		FreeRTOS_inet_ntoa( ulGatewayAddress, GATWAY_used );
		DEBUGOUT( "Gateway IP Address: %s\r\n", GATWAY_used );

		/* Convert the IP address of the DNS server to a string then print it out. */
		FreeRTOS_inet_ntoa( ulDNSServerAddress, DNS_used );
		DEBUGOUT( "DNS server IP Address: %s\r\n", DNS_used );

		if( xTasksAlreadyCreated == pdFALSE ){
			xTasksAlreadyCreated = pdTRUE;
			xTaskCreate(task_IOT, "t_IOT", configMINIMAL_STACK_SIZE*16, NULL, 1, NULL );
		}
	}
    else{
    	DEBUGOUT("Queda conexão\n");
    }
}
eDHCPCallbackAnswer_t xApplicationDHCPHook( eDHCPCallbackPhase_t eDHCPPhase, uint32_t ulIPAddress ){
	eDHCPCallbackAnswer_t eAnswer = eDHCPContinue;
	int8_t cBuffer[ 16 ];

	const char *name = "Unknown";
	switch( eDHCPPhase )
	{
	case eDHCPPhasePreDiscover:
	{
		/* The library is about to look for a DHCP server. */
		name = "Discover";            /* Driver is about to send a DHCP discovery. */
		//          eAnswer = eDHCPUseDefaults;
	}
	break;
	case eDHCPPhasePreRequest:
	{
		/* The DHCP server has made an offer to use `ulIPAddress` */
		name = "Request";            /* Driver is about to request DHCP an IP address. */
		//          eAnswer = eDHCPUseDefaults;
	}
	break;
	}
	if(_f_DHCP == 0){
		eAnswer = eDHCPStopNoChanges;

	}
	FreeRTOS_inet_ntoa( ulIPAddress, cBuffer );
	FreeRTOS_printf( ( "DHCP %s address %s %d\n", name, cBuffer, eAnswer) );
	return eAnswer;
}
/* Use by the pseudo random number generator. */
static UBaseType_t ulNextRand = 300;
UBaseType_t uxRand( void )
{
const uint32_t ulMultiplier = 0x015a4e35UL, ulIncrement = 1UL;

	/* Utility function to generate a pseudo random number. */

	ulNextRand = ( ulMultiplier * ulNextRand ) + ulIncrement;
	return( ( int ) ( ulNextRand >> 16UL ) & 0x7fffUL );
}
#if( ipconfigUSE_LLMNR != 0 ) || ( ipconfigUSE_NBNS != 0 ) || ( ipconfigDHCP_REGISTER_HOSTNAME == 1 )
	const char *pcApplicationHostnameHook( void )
	{
		/* Assign the name "FreeRTOS" to this network node.  This function will
		be called during the DHCP: the machine will be registered with an IP
		address plus this name. */
		return mainHOST_NAME;
	}
#endif
#if( ipconfigUSE_LLMNR != 0 ) || ( ipconfigUSE_NBNS != 0 )
	BaseType_t xApplicationDNSQueryHook( const char *pcName )
	{
	BaseType_t xReturn;

		/* Determine if a name lookup is for this node.  Two names are given
		to this node: that returned by pcApplicationHostnameHook() and that set
		by mainDEVICE_NICK_NAME. */
		if( strcmp( pcName, pcApplicationHostnameHook() ) == 0 )
		{
			xReturn = pdPASS;
		}
		else if( strcmp( pcName, mainDEVICE_NICK_NAME ) == 0 )
		{
			xReturn = pdPASS;
		}
		else
		{
			xReturn = pdFAIL;
		}

		return xReturn;
	}
#endif
uint32_t ulApplicationGetNextSequenceNumber( uint32_t ulSourceAddress,
													uint16_t usSourcePort,
													uint32_t ulDestinationAddress,
													uint16_t usDestinationPort )
{
	( void ) ulSourceAddress;
	( void ) usSourcePort;
	( void ) ulDestinationAddress;
	( void ) usDestinationPort;

	return uxRand();
}
void vLoggingPrintf( const char *pcFormatString, ... ){
	va_list ap;
	char msg[128];
	int len;

	va_start(ap, pcFormatString);
	len = vsnprintf(msg, sizeof(msg), pcFormatString, ap);
//	DEBUGOUT(msg);
	printf(msg);
	//UDP_SEND_DATA(msg, len);
	va_end(ap);
	//return len;
}



char http_send(void){
	Socket_t xSocketSend;
	struct freertos_sockaddr xRemoteAddress;
	size_t xAlreadyTransmitted = 0, xBytesSent = 0;
	size_t xTotalLengthToSend = 0;
	size_t xLenToSend;
	BaseType_t xSizeReceive = 0, xReceLen = 0;
	BaseType_t retCon = 0;
	char cBuffer[ 16 ];
	char receive[4*1024];
	char recBuf[512];
	char ret = 0;
	char save = 0;
	uint32_t i=0, j=0;


	xRemoteAddress.sin_port = FreeRTOS_htons( server_port );
	xRemoteAddress.sin_addr = FreeRTOS_gethostbyname((char*)server_url);
	FreeRTOS_inet_ntoa( xRemoteAddress.sin_addr, ( char * ) cBuffer );
	DEBUGOUT("IP address %s\r\n", cBuffer);

	if(xRemoteAddress.sin_addr == 0){
		ret = 1;
		return ret;
	}


	if(FreeRTOS_IsNetworkUp()){

		xSocketSend = FreeRTOS_socket(FREERTOS_AF_INET, FREERTOS_SOCK_STREAM, FREERTOS_IPPROTO_TCP);

		configASSERT( xSocketSend != FREERTOS_INVALID_SOCKET );

		retCon = FreeRTOS_connect( xSocketSend, &xRemoteAddress, sizeof( xRemoteAddress ) );

		if( retCon == 0 ){
			memset(&receive,0x00,sizeof(receive));

			xTotalLengthToSend = strlen((char*)frame);
			DEBUGOUT("SENDING ... \n");
			DEBUGOUT("'%s'",frame);
			DEBUGOUT("'%d'",strlen(frame));
			xAlreadyTransmitted = 0;

//			while( xAlreadyTransmitted < xTotalLengthToSend ){
//
//				/* How many bytes are left to send? */
				xLenToSend = xTotalLengthToSend - xAlreadyTransmitted;
				xBytesSent = FreeRTOS_send( /* The socket being sent to. */
						xSocketSend,
						/* The data being sent. */
						(char*) &(frame[ xAlreadyTransmitted ] ),
						/* The remaining length of data to send. */
						xLenToSend,
						/* ulFlags. */
						0 );
//
//				if( xBytesSent >= 0 ){
//					DEBUGOUT("\nsend: %d\n",xBytesSent);
//					/* Data was sent successfully. */
//					xAlreadyTransmitted += xBytesSent;
//				}
//				else{
//					/* Error – break out of the loop for graceful socket close. */
//					ret = 3;
//					break;
//				}
//			}

			if(ret == 0){
				IOT_SENDED += 1;
			}
			IOT_MANAGER.IOT_FRAME_SENDED = IOT_MANAGER.IOT_FRAME;

			//FreeRTOS_shutdown( xSocketSend, FREERTOS_SHUT_RDWR );
			for( ;; ){
				//memset(&recBuf,0x00,sizeof(recBuf));
				/* Receive another block of data into the cRxedData buffer. */
				xReceLen = FreeRTOS_recv( xSocketSend, recBuf, sizeof(recBuf), 0 );

				if( xReceLen > 0 )
				{
					/* Data was received, process it here. */
					//prvPorcessData( cRxedData, lBytesReceived );
					for(i=0; i<xReceLen; i++){
						receive[xSizeReceive+i] = recBuf[i];
					}
					//recBuf[xReceLen] = 0x00;
					DEBUGOUT("->: %d\n",xReceLen);
					//DEBUGOUT("->: %d '%s'\n",xReceLen,recBuf);
					//strcat(&receive,&recBuf);
					//receive[xSizeReceive] = recBuf;
					xSizeReceive += xReceLen;
				}
				else if( xReceLen == 0 )
				{
					/* No data was received, but FreeRTOS_recv() did not return an error.
			            Timeout? */
					DEBUGOUT("Timeout\n");
				}
				else
				{
					DEBUGOUT("Shutdown\n");
					/* Error (maybe the connected socket already shut down the socket?).
			            Attempt graceful shutdown. */
					//FreeRTOS_shutdown( xSocket, FREERTOS_SHUT_RDWR );
					FreeRTOS_shutdown( xSocketSend, FREERTOS_SHUT_RDWR );
					break;
				}
			}
			//memset(&recBuf,0x00,sizeof(recBuf));
			while( (xReceLen = FreeRTOS_recv( xSocketSend, recBuf, sizeof(recBuf), 0 )) >= 0 ){
				//vTaskDelay(pdMS_TO_TICKS( 250 ) );
				//xSizeReceive += xReceLen;
				DEBUGOUT("->: %d \n",xReceLen);
			}
			DEBUGOUT("recv: %d\n'%s'\n",xSizeReceive,receive);

			/* The socket has shut down and is safe to close. */
			FreeRTOS_closesocket( xSocketSend );
			DEBUGOUT("close socket\n");

			if(xSizeReceive > 0x00){
				for(i=0; i<strlen(receive); i++){
					if(receive[i]=='{'){save=1;}
					if(save && j<sizeof(response)){response[j] = receive[i]; j++;}
				}
				response[j]=0x00;
				response_SERVER();
			}
		}
	}
	else{
		ret = 2;
		DEBUGOUT("error send: %d \n", retCon);
	}

	return ret;
}
char TCP_log(char *buffer){
	Socket_t xSocketSend;
	struct freertos_sockaddr xRemoteAddress;
	size_t xAlreadyTransmitted = 0, xBytesSent = 0;
	size_t xTotalLengthToSend = 0;
	size_t xLenToSend;
	BaseType_t xReceLen = 0;
	BaseType_t retCon = 0;
	char ret = 0;
	char recBuf[512];
	char sendBuf[1024];
	char buff_rec[50];

	(void)buffer;

	snprintf((char*)server_url,sizeof(server_url),"api.tago.io");
	server_port = 80;

	json_clear();
	json_openMatrix();

	insertINT_json_body_TAGO("IOT_SENDED",IOT_SENDED);
	insertINT_json_body_TAGO("IOT_TIME_ERROR",IOT_TIME_ERROR);
	insertINT_json_body_TAGO("IOT_ERROR",IOT_ERROR);
	insertINT_json_body_TAGO("IOT_FRAME",IOT_MANAGER.IOT_FRAME);
//	snprintf(buff_rec,sizeof(buff_rec),"%s",Config.NUM_SERIE);
//	insert_json_body_TAGO("TESTE_STRING",buff_rec);

	json_closeMatrix();


	snprintf(sendBuf, sizeof(sendBuf),
			"POST /data HTTP/1.1\r\n"
			"Host: api.tago.io\r\n"
			"content-type: application/json\r\n"
			"content-length: %d\r\n"
			"device-token: c7f71928-9510-4cda-9c69-29a0e35d44b9\r\n\r\n"
			"%s",strlen(json_body),json_body);


//	xRemoteAddress.sin_port = FreeRTOS_htons( 1500 );
//	xRemoteAddress.sin_addr = FreeRTOS_gethostbyname("192.168.222.192");

	/*Envia log para a Tago*/
	xRemoteAddress.sin_port = FreeRTOS_htons( 80 );
	xRemoteAddress.sin_addr = FreeRTOS_gethostbyname("api.tago.io");

	if(xRemoteAddress.sin_addr == 0){
		ret = 1;
		return ret;
	}


	if(FreeRTOS_IsNetworkUp()){

		xSocketSend = FreeRTOS_socket(FREERTOS_AF_INET, FREERTOS_SOCK_STREAM, FREERTOS_IPPROTO_TCP);

		configASSERT( xSocketSend != FREERTOS_INVALID_SOCKET );

		retCon = FreeRTOS_connect( xSocketSend, &xRemoteAddress, sizeof( xRemoteAddress ) );

		DEBUGOUT("SEND ...\n");
		DEBUGOUT("'%s'\n",sendBuf);
		if( retCon == 0 ){

			xTotalLengthToSend = strlen((char*)sendBuf);
			xAlreadyTransmitted = 0;

			while( xAlreadyTransmitted < xTotalLengthToSend ){

				/* How many bytes are left to send? */
				xLenToSend = xTotalLengthToSend - xAlreadyTransmitted;
				xBytesSent = FreeRTOS_send( /* The socket being sent to. */
						xSocketSend,
						/* The data being sent. */
						(char*) &(sendBuf[ xAlreadyTransmitted ] ),
						/* The remaining length of data to send. */
						xLenToSend,
						/* ulFlags. */
						0 );

				DEBUGOUT("xBytesSent %d\n",xBytesSent);

				if( xBytesSent >= 0 ){
					/* Data was sent successfully. */
					xAlreadyTransmitted += xBytesSent;
				}
				else{
					/* Error – break out of the loop for graceful socket close. */
					ret = 3;
					break;
				}
			}

			DEBUGOUT("RECEIVE ...\n");
			FreeRTOS_shutdown( xSocketSend, FREERTOS_SHUT_RDWR );
			while( (xReceLen = FreeRTOS_recv( xSocketSend, recBuf, sizeof(recBuf), 0 )) >= 0 ){
				//vTaskDelay(pdMS_TO_TICKS( 250 ) );
				//xSizeReceive += xReceLen;
				DEBUGOUT("->: %d \n",xReceLen);
			}
			DEBUGOUT("REC: '%s'\n",recBuf);

			/* The socket has shut down and is safe to close. */
			FreeRTOS_closesocket( xSocketSend );
			DEBUGOUT("CLOSE\n");
		}
	}
	else{
		ret = 2;
		DEBUGOUT("error send: %d \n", retCon);
	}

	return ret;
}







void GPIO_Set(uint8_t pin, uint8_t state){
	if(!state){
		LPC_GPIO2->CLR |= (1<<pin);
	}
	else{
		LPC_GPIO2->SET |= (1<<pin);
	}
}

void Buz_Init(void){
	/*LPC_IOCON->P0_25 = 0x00;
	LPC_GPIO0->DIR |= (1<<25);*/
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 25, IOCON_FUNC0);
}
void Buz_Set(uint8_t pState){
	if(pState){
		LPC_GPIO->CLR|=(1<<25);
	}
	else{
		LPC_GPIO->SET|=(1<<25);
	}
}


volatile uint8_t _f_send_tcp_log = 0;
volatile char buf_log_tcp[512];
volatile uint8_t print_log_5sec = 0;
//----------------------------------------------------------------------------
//	RELOGIO E RTC 1SEG
//----------------------------------------------------------------------------


/*
*Biblioteca para validacao de datas
*
*Retorna 0 caso a data seja correta
*e 1 caso a data esteja incorreta
*
*Angelito M. Goulart
*Setembro/2009
*
*/
char valida_data(uint8_t dia, uint8_t mes, uint16_t ano){
	if ((dia >= 1 && dia <= 31) && (mes >= 1 && mes <= 12) && (ano >= 1900 && ano <= 2100)) //verifica se os numeros sao validos
	{
		if ((dia == 29 && mes == 2) && ((ano % 4) == 0)) //verifica se o ano e bissexto
		{
			return 0;
		}
		if (dia <= 28 && mes == 2) //verifica o mes de feveireiro
		{
			return 0;
		}
		if ((dia <= 30) && (mes == 4 || mes == 6 || mes == 9 || mes == 11)) //verifica os meses de 30 dias
		{
			return 0;
		}
		if ((dia <=31) && (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes ==8 || mes == 10 || mes == 12)) //verifica os meses de 31 dias
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
	else
	{
		return 1;
	}
}
char week_day(uint8_t dia, uint8_t mes, uint16_t ano){
	int adjustment, mm, yy;

	adjustment = (14 - mes) / 12;
	mm = mes + 12 * adjustment - 2;
	yy = ano - adjustment;
	return (dia + (13 * mm - 1) / 5 +
		yy + yy / 4 - yy / 100 + yy / 400) % 7;
}

/********************************************************************
 * Valida o formato da hora
 *
 *	@param
 *		uint8_t hora - hora
 *		uint8_t min	 - minuto
 *		uint8_t sec	 - segundo
 *
 * 	@return
 * 		0 - sucess
 * 		1 - error
 *
 ********************************************************************/
char valida_hora(uint8_t hora, uint8_t min, uint8_t sec){

	if(hora>23){return 1;}
	if(min>59){return 1;}
	if(sec>59){return 1;}

	return 0;
}
void RTC_IRQHandler(void){

	if (Chip_RTC_GetIntPending(LPC_RTC, RTC_INT_COUNTER_INCREASE)){


		if(IOT_MANAGER._c_wait_send > 0)IOT_MANAGER._c_wait_send--;
		if(IOT_MANAGER._c_time_out_response > 0)IOT_MANAGER._c_time_out_response--;

		Chip_RTC_ClearIntPending(LPC_RTC, RTC_INT_COUNTER_INCREASE);
	}
}


volatile uint8_t _c_live = 0;


/*	ETHERNET
 */
void insert_json_body_TAGO(char* pVar, char* pValue){
	json_openContainer();
	json_insertStr("variable", pVar);
	json_insertStr("value", pValue);
	json_closeContainer();
}
void insertINT_json_body_TAGO(char* pVar, int pValue){
	json_openContainer();
	json_insertStr("variable", pVar);
	json_insertInt("value", pValue);
	json_closeContainer();
}
void make_frame_IOT(void){
	uint8_t i = 0;

	if(IOT_MANAGER._c_time_out_response > 0 || IOT_MANAGER.flags.send_frame==1 || IOT_MANAGER.flags.lock == 1){return ;}
	IOT_MANAGER.flags.lock = 1;
	IOT_LINE_PASS = 0;


	IOT_MANAGER.IOT_FRAME = IOT_TAGO;

	IOT_MANAGER.flags.send_frame = 1;
	IOT_MANAGER.flags.send_success = 0;

	switch(IOT_MANAGER.IOT_FRAME){

	case IOT_TAGO:{
		snprintf((char*)server_url,sizeof(server_url),"api.tago.io");
		server_port = 80;

		json_clear();
		json_openMatrix();
		for(i=0; i<4; i++){
			insert_json_body_TAGO("cam","10");
			insert_json_body_TAGO("ir","10");
//			insert_json_body_TAGO("board","10");
//			insert_json_body_TAGO("setp","10");
//			insert_json_body_TAGO("erro","10");
//			insert_json_body_TAGO("porta","10");
//			insert_json_body_TAGO("tensao","10");
//			insert_json_body_TAGO("operacao","10");
//			insert_json_body_TAGO("grupo","10");
//			insert_json_body_TAGO("receita","10");
		}
		json_closeMatrix();


		snprintf(frame, sizeof(frame),
				"POST /data HTTP/1.1\r\n"
				"Host: %s\r\n"
				"content-type: application/json\r\n"
				"content-length: %d\r\n"
				"device-token: c7f71928-9510-4cda-9c69-29a0e35d44b9\r\n\r\n"
				"%s",server_url,strlen(json_body),json_body);

	}break;



	default:
		IOT_MANAGER.flags.send_frame = 0;
	}

	IOT_MANAGER.flags.lock = 0;

}

void error_SERVER(int8_t pEr){
	DEBUGOUT("\r\nerror Server %d\r\n",pEr);
	IOT_MANAGER._c_time_out_response = 0;
	memset(&frame,0x00,sizeof(frame));
	IOT_MANAGER.task.run = 0;
	if(pEr == -10){
//		lwip_init();	//dá hardfaul em sys_timeouts_init()

//		/* Add netif interface for lpc17xx_8x */
//		memset(&lpc_netif, 0, sizeof(lpc_netif));
//		if (!netif_add(&lpc_netif, &ipaddr, &netmask, &gw, NULL, lpc_enetif_init,
//				tcpip_input)) {
//			DEBUGSTR("Net interface failed to initialize\r\n");
//			while(1);
//		}
//		netif_set_default(&lpc_netif);
//		netif_set_up(&lpc_netif);
//
//		if(Config._f_DHCP == 1){
//			dhcp_start(&lpc_netif);
//		}
//		lpc_phy_init(true, msDelay);
//		while(1);
	}
}

void response_SERVER(void){
	DEBUGOUT("'%s'\n",response);

	IOT_MANAGER._c_wait_send = 5;
	IOT_ERROR = SIOT_ALL_OK;

	_c_ETH_Tempo_medio_resposta += _c_ETH_Tempo_resposta;
	_c_ETH_Tempo_medio_resposta /= 2;

	IOT_TIME_ERROR = 0;

	if (strstr((char*)response, "retorno\":\"true")!=NULL){
		json_parse(response, 0);
		IOT_MANAGER.flags.send_success = 1;
	}
	else {
		switch(IOT_MANAGER.IOT_FRAME_SENDED){
		case IOT_AUTHENTICATE:{
			if (strstr((char*)response, "duplicado")!=NULL){
				IOT_Config.RESET = 1;
			}
		}break;
		case IOT_RESET:{
			IOT_Config.RESET = 0;
			IOT_Config.AUTENTICATE = 0;
		}break;
		}
		IOT_ERROR = SIOT_ERRO_SERVER;
	}

	memset(&frame,0x00,sizeof(frame));
	IOT_MANAGER._c_time_out_response = 0;
}

void json_call_field(char* pVar, char* pValue, int pIndex, char pSub){
	char tmp[10];
	static uint8_t i = 0;
	DEBUGOUT("%d %d %s %s\r\n",pSub, pIndex, pVar, pValue);

//	switch(IOT_MANAGER.IOT_FRAME_SENDED){
//
//	}
}

void cbk_TCP_SEND(uint16_t pLen){
	DEBUGOUT("SEND %d\r\n",pLen);
	DEBUGOUT(frame);
	DEBUGOUT("\r\n\r\n");
//	IOT_SENDED += 1;
}






void WatchDog_Init(void){
#ifdef WATCHDOG

	//Configurações do Watchdog
//	WWDT_SetTimerConstant(500000);  //(4 x 500.000)/500.000 = 4 seg
//	WWDT_SetMode(WWDT_RESET_MODE,ENABLE);
//	WWDT_Enable(ENABLE);
//	WWDT_FeedStdSeq();
//	if (WWDT_GetStatus(WWDT_TIMEOUT_FLAG)) {_f_WatchDog=1;}
//	WWDT_ClrTimeOutFlag();

	Chip_WWDT_Init(LPC_WWDT);
	Chip_WWDT_SetTimeOut(LPC_WWDT, WDT_OSC*4);

	Chip_WWDT_SetOption(LPC_WWDT, WWDT_WDMOD_WDRESET);

	NVIC_ClearPendingIRQ(WDT_IRQn);
	NVIC_EnableIRQ(WDT_IRQn);

	Chip_WWDT_Start(LPC_WWDT);

	if(Chip_WWDT_GetStatus(LPC_WWDT)&WWDT_WDMOD_WDRESET){_f_WatchDog=1;}
	Chip_WWDT_ClearStatusFlag(LPC_WWDT, WWDT_WDMOD_WDTOF | WWDT_WDMOD_WDINT);

	_f_WatchDogFeed=0x00;
#endif
}
//----------------------------------------------------------------------------
//	BOARD INIT
//----------------------------------------------------------------------------

void BoardInit(void){

	Chip_IOCON_PinMuxSet(LPC_IOCON, LED_MOD[0].PORT, LED_MOD[0].PIN, LED_MOD[0].FUNC);
	Chip_IOCON_PinMuxSet(LPC_IOCON, LED_MOD[1].PORT, LED_MOD[1].PIN, LED_MOD[1].FUNC);

	LPC_GPIO2->DIR |= (1<<26);
	LPC_GPIO2->SET |= (1<<26);

	Chip_RTC_Init(LPC_RTC);
	Chip_RTC_CntIncrIntConfig (LPC_RTC, RTC_AMR_CIIR_IMSEC, ENABLE);
	Chip_RTC_ClearIntPending(LPC_RTC, RTC_INT_COUNTER_INCREASE);
	NVIC_EnableIRQ(RTC_IRQn);
	Chip_RTC_Enable(LPC_RTC, ENABLE);


//	__low_level_init();

}

void msDelay(uint32_t ms)
{
	vTaskDelay((configTICK_RATE_HZ * ms) / 1000);
}

//----------------------------------------------------------------------------
//	TASKS
//----------------------------------------------------------------------------
void MainTask(void);
void MainTask(void) {

//	debug_frmwrk_init();
//	_DBG("FreeRTOS EMWIN FatFS  Example runing\n\r");

	SystemCoreClockUpdate();
	Board_Init();

	BoardInit();

	vPortDefineHeapRegions(xHeapRegions);

	PLINE();
//	delayMs(2000);
	//vTaskDelay(2000);
	FreeRTOS_IPInit( ucIPAddress, ucNetMask, ucGatewayAddress, ucDNSServerAddress, ucMACAddress );
	FreeRTOS_TCP_Init = 1;
	PLINE();

	vTaskStartScheduler();


	for(;;);
}

int main(void)
{
	MainTask();
}


static void task_Led01( void *pvParameters ){
	uint8_t led_state=0;

	//_DBG("Led01 running...\n\r");
	while(1){
		GPIO_Set(26,led_state);
		led_state = !led_state;
		cont_LED01++;
		vTaskDelay(300);
	}
}

volatile uint8_t init_phy = 0;
static void task_IOT(void *pvParameters) {

	memset(&IOT_MANAGER.upload, 0x00,sizeof(IOT_MANAGER.upload));
	memset(&IOT_MANAGER.download, 0x00,sizeof(IOT_MANAGER.download));
	memset(&IOT_MANAGER.flags, 0x00,sizeof(IOT_MANAGER.flags));
	memset(&IOT_MANAGER.load, 0x00,sizeof(IOT_MANAGER.load));

	memset((void*)&frame, 0x00, sizeof(frame));

	IOT_MANAGER._c_wait_send = 0;
	IOT_MANAGER.IOT_FRAME = IOT_IDDLE;
	IOT_MANAGER._c_time_out_response = 0;
	_c_ETH_TIMER = 0;


	IOT_MANAGER.task.run = 1;
	IOT_MANAGER.task.running = 1;

	DEBUGOUT("device-token: %s\n",IOT_Config.server.TOKEN);
	DEBUGOUT("pincode: %s\n",IOT_Config.server.PINCODE);

	//Config._f_DHCP = 1;

	IOT_ERROR = SIOT_CABLE_DISCONECTED;
	IOT_MANAGER.flags.cable_disconnected = 1;

	while(1){

		if(FreeRTOS_IsNetworkUp()){
			IOT_MANAGER.flags.cable_disconnected = 0;
			IOT_ERROR = SIOT_WAIT_IP;
			IOT_ERROR = SIOT_CONECTION_FAILED;
		}
		else{
			IOT_MANAGER.flags.cable_disconnected = 1;
			IOT_ERROR = SIOT_CABLE_DISCONECTED;
		}


		if(IOT_TIME_ERROR > (5*60) && IOT_WAIT_CLOSE_ALL_CONNECTIONS==0){
			IOT_WAIT_CLOSE_ALL_CONNECTIONS = 5*60;
		}

		if(_f_send_tcp_log == 1){
			TCP_log((char*)buf_log_tcp);
			_f_send_tcp_log = 0;
		}

		if(IOT_MANAGER.flags.cable_disconnected == 0 && IOT_WAIT_CLOSE_ALL_CONNECTIONS == 0){
			make_frame_IOT();

			if(IOT_MANAGER.flags.send_frame != 0 && IOT_MANAGER._c_wait_send == 0 && IOT_MANAGER.IOT_FRAME != IOT_IDDLE && IOT_MANAGER._c_time_out_response == 0){
				if (TCP_log("hi") == 0){
					/* tempo minimo entre um pacote e outro*/
					_c_ETH_Tempo_resposta = 0;

					IOT_MANAGER.flags.send_frame = 0;
					IOT_MANAGER._c_time_out_response = 5;
				}
				else{
					IOT_ERROR = SIOT_ERRO_DNS;
					vTaskDelay(configTICK_RATE_HZ);
				}
			}
			else{
				DEBUGOUT("\r\nwait unlock iot:%d st:%d s:%d t:%d\r\n",IOT_MANAGER.IOT_FRAME,IOT_MANAGER._c_time_get_status,IOT_MANAGER._c_wait_send,IOT_MANAGER._c_time_out_response);
				vTaskDelay(configTICK_RATE_HZ);
			}
//			switch(_f_NET_STATUS){
//			case 0: break;
//			case 1:{
////				TAGO_TIME_OUT = 5;
////				TAGO_SD_W_SEND = 1; //log do que foi enviado
////				if (TAGO_SEND_BACKUP){TAGO_SENDED_BACKUP=1; TAGO_SEND_BACKUP=0;}
////				if (tcp_setup((const char*)server)==ERR_OK) _f_NET_STATUS = 2;
//
//				make_frame_IOT();
//
//				if(IOT_MANAGER.flags.send_frame != 0 && IOT_MANAGER._c_wait_send == 0 && IOT_MANAGER.IOT_FRAME != IOT_IDDLE && IOT_MANAGER._c_time_out_response == 0){
//					if (http_send() == 0){
//						_f_NET_STATUS = 2;
//						/* tempo minimo entre um pacote e outro*/
//						_c_ETH_TIMER=5;
//						_c_ETH_Tempo_resposta = 0;
//
//						IOT_MANAGER.flags.send_frame = 0;
//						IOT_MANAGER._c_time_out_response = 5;
//					}
//					else{
//						IOT_ERROR = SIOT_ERRO_DNS;
//						vTaskDelay(configTICK_RATE_HZ);
//					}
//				}
//				else{
//					DEBUGOUT("\r\nwait unlock iot:%d st:%d s:%d t:%d\r\n",IOT_MANAGER.IOT_FRAME,IOT_MANAGER._c_time_get_status,IOT_MANAGER._c_wait_send,IOT_MANAGER._c_time_out_response);
//					vTaskDelay(configTICK_RATE_HZ);
//				}
//
//			}break;
//			default:{
//				if(_c_ETH_TIMER==0 && IOT_MANAGER._c_time_out_response == 0){
//					_f_NET_STATUS=1;
//					DEBUGOUT("\r\nResend \r\n");
//					if(IOT_MANAGER.flags.send_success == 0){
//						switch(IOT_MANAGER.IOT_FRAME_SENDED){
//						case IOT_UPLOAD_GRUPO:{
//							IOT_MANAGER.upload._c_index_grupo -= IOT_MANAGER.upload._c_aux_grupo;
//						}break;
//
//						case IOT_UPLOAD_RECEITA:{
//							if(IOT_MANAGER.flags.next_grupo == 1){
//								IOT_MANAGER.upload._c_index_grupo --;
//							}
//							IOT_MANAGER.upload._c_index_receitas -= IOT_MANAGER.upload._c_aux_receita;
//							IOT_MANAGER.upload._c_index_receita -= IOT_MANAGER.upload._c_aux_receita;
//						}break;
//						}
//					}
//					IOT_MANAGER.buf_load = 0;
//				}
//				vTaskDelay(configTICK_RATE_HZ);
//			}
//			}
		}
		else{//CABO DESCONECTADO SEMPRE SALVA BACKUP
//			if(_f_SEND_TAGO){TAGO_SD_W_BACKUP=1; _f_SEND_TAGO=0;}
			vTaskDelay(configTICK_RATE_HZ);
		}

		IOT_MANAGER._c_timer_task = 5;
	}
	DEBUGSTR("Closing IOT Task\r\n");

	vTaskDelete(xtask_IOT);
}

static void task_Ms( void *pvParameters ){
	uint16_t ms300 = 0;
	uint8_t led_state=0;

	while(1){
		_c_ETH_Tempo_resposta++;
		ms300++;

		if(ms300>300){
			GPIO_Set(26,led_state);
			led_state = !led_state;
			cont_LED01++;
			ms300=0;
		}
		cont_LED01++;
		vTaskDelay(1);
	}
}

