/*
 * App.h
 *
 *  Created on: 26 de jan de 2017
 *      Author: Ulysses
 */

#ifndef INC_APP_H_
#define INC_APP_H_


#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "chip.h"
#include "board_types.h"

#include "JSON.h"

#include "projdefs.h"
#include "FreeRTOS_Sockets.h"
#include "FreeRTOS_IP.h"
#include "FreeRTOS_DHCP.h"


#include <stdarg.h>
#include <stdio.h>
#include <string.h>

/* Define a name that will be used for LLMNR and NBNS searches. */
#define mainHOST_NAME				"RTOSDemo"
#define mainDEVICE_NICK_NAME		"windows_demo"

#ifdef DEBUG
#	define  Debug_Log(...)       //UART_Printf(__VA_ARGS__)
#else
#	define  Debug_Log(...)
#endif


extern PIN_COM_TypeDef LED_MOD[2];


//===================================================================
//	 ETHERNET E IOT
//===================================================================
volatile uint8_t _f_DHCP = 1;
volatile uint8_t _f_CABLE_DISCONNECTED = 1;
volatile uint8_t _f_ETH_SEND = 0;
volatile uint8_t _c_ETH_TIMER = 0;
volatile uint8_t FreeRTOS_TCP_Init = 0;


volatile uint16_t _c_ETH_Tempo_resposta = 0;
volatile uint16_t _c_ETH_Tempo_medio_resposta = 0;

struct{
	uint8_t _c_time_out_response;
	uint8_t _c_wait_send;
	uint8_t IOT_FRAME;
	uint8_t IOT_FRAME_SENDED;

	uint16_t _c_time_get_status;


	struct{
		uint8_t  _c_max_grupos;
		uint8_t  _c_index_grupo;

		uint16_t _c_max_receitas;
		uint16_t _c_index_receita;

		uint16_t _c_received;

		union{
			uint8_t buf_downlad;
			struct{
				uint8_t save:1;
				uint8_t delete:1;
				uint8_t replace:1;
				uint8_t load:1;
				uint8_t passos:1;
			}flags;
		};
	} download;

	struct{
		uint16_t _c_total_grupos;
		uint16_t _c_total_receitas;

		uint16_t _c_index_grupo;
		uint16_t _c_index_receita;
		uint16_t _c_index_receitas;
		uint16_t _c_aux_grupo;
		uint16_t _c_aux_receita;
	} upload;


	union{
		uint8_t buf_backup;
		struct{
			uint8_t save_sd:1;
			uint8_t send_sd:1;
			uint8_t del_register_sd:1;

		}backup;
	};

	union{
		uint32_t buf_flags;
		struct {
			uint32_t cable_disconnected:1;

			uint32_t need_upload:1;
			uint32_t info_upload:1;
			uint32_t end_upload_grp:1;
			uint32_t end_upload_rec:1;
			uint32_t end_upload_conf:1;
			uint32_t end_upload:1;

			uint32_t need_download:1;
			uint32_t info_download:1;
			uint32_t end_download_grp:1;
			uint32_t end_download_rec:1;
			uint32_t end_download_conf:1;
			uint32_t end_download:1;

			uint32_t send_frame:1;
			uint32_t send_success:1;

			uint32_t next_grupo:1;

			uint32_t lock:1;
		} flags;
	};

	union{
		uint8_t buf_load;
		struct{
			uint8_t grupos:1;
			uint8_t receitas:1;
			uint8_t totais:1;
			uint8_t done:1;
			uint8_t end:1;
			uint8_t load:1;
		} load;
	};

	uint8_t _c_timer_task;
	union {
		uint8_t buf_task;
		struct{
			uint8_t run:1;
			uint8_t running:1;
			uint8_t start:1;

		}task;
	};

}IOT_MANAGER;

volatile uint16_t IOT_TIME_ERROR = 0;
volatile uint16_t IOT_SENDED = 0;
volatile uint8_t IOT_ERROR = 0;
volatile uint8_t IOT_CHANGE_IP_CONFIG = 0;
volatile uint8_t IOT_TIMER_CHANGE_IP = 20;
volatile uint16_t IOT_WAIT_CLOSE_ALL_CONNECTIONS = 0;
enum{
	SIOT_CABLE_DISCONECTED,
	SIOT_WAIT_IP,
	SIOT_ERRO_SERVER,
	SIOT_ERRO_DNS,
	SIOT_CONECTION_FAILED,
	SIOT_ALL_OK
};
volatile uint8_t IOT_LINE_PASS = 0;

volatile char server_url[100]		__attribute__ ((section (".TEMP_APP")));
volatile uint16_t server_port = 80;
volatile char frame[4*1024]		__attribute__ ((section (".TEMP_APP")));
volatile char json_body[4*1024] __attribute__ ((section (".TEMP_APP")));
volatile char sd_backup[1*1024*1024] __attribute__ ((section (".TEMP_APP")));
volatile char response[1*1024*1024]	__attribute__ ((section (".TEMP_APP")));

char json_variable[20];
char json_value[40];

char IP_used[20] = {"0.0.0.0"};
char GATWAY_used[20] = {"0.0.0.0"};
char MASK_used[20] = {"0.0.0.0"};
char DNS_used[20] = {"0.0.0.0"};



enum{
	IOT_IDDLE,

	IOT_AUTHENTICATE,
	IOT_RESET,

	IOT_GET_STATUS,

	IOT_UPLOAD_INFO,
	IOT_UPLOAD_GRUPO,
	IOT_UPLOAD_RECEITA,
	IOT_UPLOAD_CONFIG,
	IOT_UPLOAD_FIM,

	IOT_DOWNLOAD_INFO,
	IOT_DOWNLOAD_GRUPO,
	IOT_DOWNLOAD_RECEITA,
	IOT_DOWNLOAD_CONFIG,
	IOT_DOWNLOAD_FIM,

	IOT_TAGO
};

typedef struct{
	struct{
		char TOKEN[37];
		char PINCODE[5];
		uint32_t VERSAO_SERVER;
		uint32_t VERSAO_LOCAL;
	} server;
	union{
		uint8_t buf;
		struct{
			uint8_t AUTENTICATE:1;
			uint8_t RESET:1;
			uint8_t	Updated:1;
		};
	};
} t_IOT_CONF;

t_IOT_CONF IOT_Config;



void response_SERVER(void);
void insertINT_json_body_TAGO(char* pVar, int pValue);

#endif /* INC_APP_H_ */


