/**********************************************************************
 * $Id$		Files.h			2017-05-02
 * @file	Files.h
 * @brief	Contem os prototipos das funcoes de Files.c e as listas de receitas
 * @version	1.0
 * @date	02. Maio. 2017
 * @author	Ulysses C. Fonseca
 *
 *
 *
 **********************************************************************/
/*********************************************************************
 * 	ERRATA
 *
 *
 *
 **********************************************************************/

#ifndef __FILES_H__
#define __FILES_H__

#include "Files_tipos.h"
#include "Files_tipos_old.h"


//VERSOES ANTIGAS
volatile s_Lista_EXPORT_0x0001 Lista_EXP_0x0001;
volatile s_Lista_NACIONAL_0x0001 Lista_NAC_0x0001;
volatile s_Config_NACIONAL Config_NAC;


//volatile s_Lista GRUPOS[8];
volatile s_Lista Lista;			//LISTA TEMPORARIA
volatile s_Lista Grupo_Sel;		//UTILIZADO PARA EDICAO DE GRUPO
volatile s_Lista Rec_Sel;		//UTILIZADO PARA EDICAO DE RECEITA
volatile s_RecBravo RecBravo;	//RECEITA EM EXECUCAO OU EM EDICAO

volatile s_Registros Contadores[11];	//CONTADORES CLASSIFICADOS


void strip(char *s);


void strRemNLine(char *str);

char checkFile(char *pPath);
char createFile(char *pPath);
char createFolder(char *pPath);
char deleteFile(char *pPath);
char deleteFolder(char *pPath);
char deleteFolderRecursive(char *pPath);
char copy_file(char *f_origem, char *f_destino);
char writeFile(char *pPath, char *pBuffer, uint32_t pLength, uint16_t pPos, uint8_t pCripto);
char readFile(char *pPath, char *pBuffer, uint32_t pLength, uint16_t pPos, uint8_t pCripto);

char addRegistro(char *pPath, char *pBuffer, uint32_t pLength);
char readRegistro(char *pPath, char *pBuffer, uint32_t pLength, uint32_t pPos);

char loadRec(char *pPath, s_RecBravo *pRec);
char saveRec(char *pPath, s_RecBravo *pRec);



char addItemList(char *pPath, s_Lista pList);
char uptItemList(char *pPath, s_Lista pList, uint8_t pIndex);
char reposItemList(char *pPath, uint16_t pPos, uint16_t pNewPos);
char deleteItemList(char *pPath, uint16_t pPos);
uint32_t readNextId(char *pPath);
uint32_t readTotalId(char *pPath);
char readItemList(char *pPath, s_Lista *pList, uint16_t pPos);
char readItemList_other(char *pPath, void *pList, uint32_t pSize, uint16_t pPos);


int fupdate_check_info(uint16_t *Num_Arquivos, uint32_t *Tamanho_Bytes);
void _folder_recursive(char *path);
char _check_files_USB(char *file);
char _copy_files_Update(char *file);
int UpdateCheck_Files_Upgrade(uint8_t *pUpdateScr, uint8_t pUpdate, uint16_t *pTotalFiles);
void set_free_space_sd(uint64_t pFreeSpace);

char fcheck_boot(void);

char fget_size_with_struct_list(char *file_1, uint16_t pSize);

#endif
