@echo off 
cls 
Set /p port=Digite o numero da porta serial a ser utilizada? COM(x): 
:p1 
echo. 
echo BOOTLOADER LPC1788
echo.
FM.EXE COM(%port%,57600) DEVICE(LPC1778,12,0) ERASE(DEVICE,PROTECTISP) HARDWARE(ASSERT) READSIGNATURE HEXFILE(BootLoader_LPC1788_V104.hex,NOCHECKSUMS,NOFILL,PROTECTISP) VERIFY(BootLoader_LPC1788_V104.hex,NOCHECKSUMS)
echo. 
Set /p choice=Deseja gravar novamente? (s/n): 
if '%choice%' == 'n' (
goto p2
) 
if '%choice%' == 'N' (
goto p2 
) 
cls 
goto p1 
:p2 
cls