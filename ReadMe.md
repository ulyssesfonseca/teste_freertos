﻿## Sobre
Este projeto em conjunto com a placa [Power Pratica] (http://192.168.222.62:81/!/#HW000025_PRATICA_POWER_PRATICA) se torna o projeto **_COPA/ROCKET_**.
É um equipamento de microondas ultra-rápido, com resistencias integradas para maior performance.
Esta aplicação utiliza bootloader, deslocando assim a aplicação na memória flash do chip, o início da aplicação se dá no endereço 0x6000. Os arquivos de bootloader para gravação estão contido na pasta _Project Output/**target**/Bootloader_



## Compilação
Este projeto deve ser compilado utilizando a IDE MCUXpresso v11.0.1 ou superior.

## Bibliotecas
Abaixo são listadas algumas bibliotecas utilizadas neste projeto e suas respectivas versões e/ou ano.

* LPCOpen - 2014
* LPCUSBLib - 2012
* FatFS - R0.13C - 2018
* FreeRTOS - V10.2.1 - 2019
* FreeRTOS+TCP - V2.0.11 - 2017
* CPT_FT5216 - 2017
* EmbNet - 1.0.7.4 - 2019
* CAN - 1.0.3 - 2017
* MCP9803 - 0.0.1 - 2018
* JSON - 1.0.0 - 2019


## Informações adicionais

**Heap**

Há alocado 100KB para heap neste projeto, atualmente são utilizados 46KB aproximadamente 45%. Em alterações futuras este detalhe deve ser atentado pois o estouro da heap pode causar bugs e problemas inesperados.
O ideal é que fique livre uma porcentagem da heap por segurança.
É utilizada a _heap_5_ para o FreeRTOS, onde a heap fica localizada na memória RAM externa.
Para debug da heap é necessário fazer alterações no código para visualizar no debug da IDE, para isto acesse o [MCUXpresso_IDE_FreeRTOS_Debug_Guide.pdf](https://www.nxp.com/docs/en/quick-reference-guide/MCUXpresso_IDE_FreeRTOS_Debug_Guide.pdf) no tópico _Required Source Code Changes_.
Neste projeto já está definidas as alterações de debug, portanto devem ser feitas somente as seguintes alterações no arquivo _FreeRTOSConfig.h_.
>define configINCLUDE_FREERTOS_TASK_C_ADDITIONS_H	1

>define configUSE_TRACE_FACILITY	1

------------

**FreeRTOS**

Este projeto utiliza o FreeRTOS como sistema de tempo real, v10.2.1, com suporte dinâmico de alocação de memória. Por este ultimo motivo deve ser atento ao item anterior _heap_.
São criadas na aplicação 5 tasks sendo estas:
* task_MainGUI
* task_Ms
* task_USB
* task_CTRLSys
* task_IOT

Porém ainda são criadas mais 4 tasks internamente, sendo 2 criadas pelo FreeRTOS para controle interno e mais 2 para interface ethernet.

------------

**FreeRTOS+TCP**

Como pilha de TCP é utilizada a pilha _FreeRTOS+TCP_ mantida pela _AWS_, e pode ser encontrada no repositório oficial do [github](https://github.com/aws/amazon-freertos) ou diretamente no site [FreeRTOS](https://www.freertos.org/FreeRTOS-Plus/FreeRTOS_Plus_TCP/index.html).
Este utiliza o driver não oficial para o LPC1788 adaptado do projeto LPC40x8. Toda implementação e auxilio disponibilizado pela comunidade FreeRTOS pode ser encontrada nos tópicos:
* [FreeRTOS+TCP for LPC17xx](https://forums.freertos.org/t/freertos-tcp-for-lpc17xx/8377)
* [FreeRTOS+TCP DHCP or Static in runtime](https://forums.freertos.org/t/freertos-tcp-dhcp-or-static-in-runtime/8445/11)
* [FreeRTOS_recv not receive more than 1000 bytes](https://forums.freertos.org/t/freertos-recv-not-receive-more-than-1000-bytes/8454/2)

------------

**FatFS**

Para sistema de arquivos SD e USB é utilizada a biblioteca FatFS mantida pela [elm-chan](http://elm-chan.org/fsw/ff/00index_e.html).

------------
