################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lwip/src/core/ipv4/autoip.c \
../lwip/src/core/ipv4/icmp.c \
../lwip/src/core/ipv4/igmp.c \
../lwip/src/core/ipv4/inet.c \
../lwip/src/core/ipv4/inet_chksum.c \
../lwip/src/core/ipv4/ip.c \
../lwip/src/core/ipv4/ip_addr.c \
../lwip/src/core/ipv4/ip_frag.c 

OBJS += \
./lwip/src/core/ipv4/autoip.o \
./lwip/src/core/ipv4/icmp.o \
./lwip/src/core/ipv4/igmp.o \
./lwip/src/core/ipv4/inet.o \
./lwip/src/core/ipv4/inet_chksum.o \
./lwip/src/core/ipv4/ip.o \
./lwip/src/core/ipv4/ip_addr.o \
./lwip/src/core/ipv4/ip_frag.o 

C_DEPS += \
./lwip/src/core/ipv4/autoip.d \
./lwip/src/core/ipv4/icmp.d \
./lwip/src/core/ipv4/igmp.d \
./lwip/src/core/ipv4/inet.d \
./lwip/src/core/ipv4/inet_chksum.d \
./lwip/src/core/ipv4/ip.d \
./lwip/src/core/ipv4/ip_addr.d \
./lwip/src/core/ipv4/ip_frag.d 


# Each subdirectory must supply rules for building sources it contributes
lwip/src/core/ipv4/%.o: ../lwip/src/core/ipv4/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -DCORE_M3 -D__LPC177X_8X__ -DUSE_SD -DUSE_USB -D__USE_LPCOPEN -D__CODE_RED -DUSB_CAN_BE_HOST -DUSB_HOST_ONLY -DSVNREV='"$(shell svn info --show-item=revision E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -Dold_SVN_REV='"$(shell svnversion -n E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -DSVN_REV='"$(shell svn info -r HEAD --show-item revision)"' -D__REDLIB__ -I"E:\PROJETOS\CLIENTES\PRATICA\SW000030_PRATICA_IHM_5_POL_TFT-COPA_CONECTADO\emwin\GUI" -I"E:\PROJETOS\CLIENTES\PRATICA\SW000030_PRATICA_IHM_5_POL_TFT-COPA_CONECTADO\emwin\Inc" -I"E:\PROJETOS\CLIENTES\PRATICA\SW000030_PRATICA_IHM_5_POL_TFT-COPA_CONECTADO\emwin\Config" -I"E:\PROJETOS\CLIENTES\PRATICA\SW000030_PRATICA_IHM_5_POL_TFT-COPA_CONECTADO\emwin\System\HW" -I"E:\PROJETOS\CLIENTES\PRATICA\SW000030_PRATICA_IHM_5_POL_TFT-COPA_CONECTADO\app\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\SW000030_PRATICA_IHM_5_POL_TFT-COPA_CONECTADO\board\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\SW000030_PRATICA_IHM_5_POL_TFT-COPA_CONECTADO\mcu\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\SW000030_PRATICA_IHM_5_POL_TFT-COPA_CONECTADO\gui_emb\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\SW000030_PRATICA_IHM_5_POL_TFT-COPA_CONECTADO\panel_touch_FT5216\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\SW000030_PRATICA_IHM_5_POL_TFT-COPA_CONECTADO\can\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\SW000030_PRATICA_IHM_5_POL_TFT-COPA_CONECTADO\embnet\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\SW000030_PRATICA_IHM_5_POL_TFT-COPA_CONECTADO\utilities\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\SW000030_PRATICA_IHM_5_POL_TFT-COPA_CONECTADO\freertos\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\SW000030_PRATICA_IHM_5_POL_TFT-COPA_CONECTADO\mcp9803\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\SW000030_PRATICA_IHM_5_POL_TFT-COPA_CONECTADO\lpcusblib\Drivers\USB" -I"E:\PROJETOS\CLIENTES\PRATICA\SW000030_PRATICA_IHM_5_POL_TFT-COPA_CONECTADO\fatfs\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\SW000030_PRATICA_IHM_5_POL_TFT-COPA_CONECTADO\lwip\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\SW000030_PRATICA_IHM_5_POL_TFT-COPA_CONECTADO\lwip\inc\arch" -I"E:\PROJETOS\CLIENTES\PRATICA\SW000030_PRATICA_IHM_5_POL_TFT-COPA_CONECTADO\lwip\inc\lwip" -I"E:\PROJETOS\CLIENTES\PRATICA\SW000030_PRATICA_IHM_5_POL_TFT-COPA_CONECTADO\lwip\inc\ipv4" -I"E:\PROJETOS\CLIENTES\PRATICA\SW000030_PRATICA_IHM_5_POL_TFT-COPA_CONECTADO\JSON\inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


