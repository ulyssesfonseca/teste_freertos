################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../freertos/croutine.c \
../freertos/event_groups.c \
../freertos/list.c \
../freertos/queue.c \
../freertos/stream_buffer.c \
../freertos/tasks.c \
../freertos/timers.c 

OBJS += \
./freertos/croutine.o \
./freertos/event_groups.o \
./freertos/list.o \
./freertos/queue.o \
./freertos/stream_buffer.o \
./freertos/tasks.o \
./freertos/timers.o 

C_DEPS += \
./freertos/croutine.d \
./freertos/event_groups.d \
./freertos/list.d \
./freertos/queue.d \
./freertos/stream_buffer.d \
./freertos/tasks.d \
./freertos/timers.d 


# Each subdirectory must supply rules for building sources it contributes
freertos/%.o: ../freertos/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -DCORE_M3 -D__LPC177X_8X__ -DUSE_SD -DUSE_USB -D__USE_LPCOPEN -D__CODE_RED -DUSB_CAN_BE_HOST -DUSB_HOST_ONLY -DSVNREV='"$(shell svn info --show-item=revision E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -Dold_SVN_REV='"$(shell svnversion -n E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -DSVN_REV='"$(shell svn info -r HEAD --show-item revision)"' -D__REDLIB__ -I"E:\PROJETOS\CLIENTES\TESTE_2\teste_freertos\app\inc" -I"E:\PROJETOS\CLIENTES\TESTE_2\teste_freertos\board\inc" -I"E:\PROJETOS\CLIENTES\TESTE_2\teste_freertos\mcu\inc" -I"E:\PROJETOS\CLIENTES\TESTE_2\teste_freertos\gui_emb\inc" -I"E:\PROJETOS\CLIENTES\TESTE_2\teste_freertos\utilities\inc" -I"E:\PROJETOS\CLIENTES\TESTE_2\teste_freertos\JSON\inc" -I"E:\PROJETOS\CLIENTES\TESTE_2\teste_freertos\freertos\include" -I"E:\PROJETOS\CLIENTES\TESTE_2\teste_freertos\FreeRTOS-Plus-TCP\include" -I"E:\PROJETOS\CLIENTES\TESTE_2\teste_freertos\FreeRTOS-Plus-TCP\portable\Compiler\GCC" -I"E:\PROJETOS\CLIENTES\TESTE_2\teste_freertos\freertos\portable\gcc\ARM_CM3" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -fmerge-constants -fmacro-prefix-map="../$(@D)/"=. -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


