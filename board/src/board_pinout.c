/*
 * board_pinout.c
 *
 *  Created on: Aug 7, 2018
 *      Author: Ulysses
 */
#include "board_pinout.h"


PIN_COM_TypeDef I2C_SDA = {IOCON_FUNC0, 0, 27};
PIN_COM_TypeDef I2C_SCL = {IOCON_FUNC0, 0, 28};


PIN_COM_TypeDef TOUCH_RESET = {IOCON_FUNC0, 0, 20};
PIN_COM_TypeDef TOUCH_WAKE = {IOCON_FUNC0 | IOCON_DIGMODE_EN | IOCON_MODE_PULLUP, 0, 23};
PIN_COM_TypeDef TOUCH_EINT = {IOCON_FUNC0 | IOCON_DIGMODE_EN | IOCON_MODE_PULLUP, 0, 24};

PIN_COM_TypeDef LCD [21] = {
		//R
		{IOCON_FUNC5 | IOCON_MODE_INACT, 2, 12},	// LCD_VD_3		R0	0
		{IOCON_FUNC7 | IOCON_MODE_INACT, 2, 6},		// LCD_VD_4		R1	1
		{IOCON_FUNC7 | IOCON_MODE_INACT, 2, 7},		// LCD_VD_5 	R2	2
		{IOCON_FUNC7 | IOCON_MODE_INACT, 2, 8},		// LCD_VD_6		R3	3
		{IOCON_FUNC7 | IOCON_MODE_INACT, 2, 9},		// LCD_VD_7		R4	4

		//G
		{IOCON_FUNC7 | IOCON_MODE_INACT, 1, 20},	// LCD_VD_10	G0	5
		{IOCON_FUNC7 | IOCON_MODE_INACT, 1, 21},	// LCD_VD_11	G1	6
		{IOCON_FUNC7 | IOCON_MODE_INACT, 1, 22},	// LCD_VD_12	G2	7
		{IOCON_FUNC7 | IOCON_MODE_INACT, 1, 23},	// LCD_VD_13	G3	8
		{IOCON_FUNC7 | IOCON_MODE_INACT, 1, 24},	// LCD_VD_14	G4	9
		{IOCON_FUNC7 | IOCON_MODE_INACT, 1, 25},	// LCD_VD_15	G5	10

		//B
		{IOCON_FUNC7 | IOCON_MODE_INACT, 2, 13},	// LCD_VD_19	B0	11
		{IOCON_FUNC7 | IOCON_MODE_INACT, 1, 26},	// LCD_VD_20	B1	12
		{IOCON_FUNC7 | IOCON_MODE_INACT, 1, 27},	// LCD_VD_21	B2	13
		{IOCON_FUNC7 | IOCON_MODE_INACT, 1, 28},	// LCD_VD_22	B3	14
		{IOCON_FUNC7 | IOCON_MODE_INACT, 1, 29},	// LCD_VD_23	B4	15

		//CONTROL
		{IOCON_FUNC7 | IOCON_MODE_INACT, 2, 0},		// LCD_PWR			16
		{IOCON_FUNC7 | IOCON_MODE_INACT, 2, 2},		// LCD_DCLK			17
		{IOCON_FUNC7 | IOCON_MODE_INACT, 2, 4},		// LCD_ENAB_M		18

		//BACKLIGHT
		{IOCON_FUNC0, 2, 27},						// BKL				19
		{IOCON_FUNC0, 1, 18},						// BKL				20
};


PIN_COM_TypeDef LED_MOD[2] = {
		{IOCON_FUNC0, 2, 26},	// RED
		{IOCON_FUNC0, 2, 27},	// GREEN
};

PIN_COM_TypeDef CAN1[2] = {
		{IOCON_FUNC1, 0, 0},	//RD1
		{IOCON_FUNC1, 0, 1},	//TD1
};

