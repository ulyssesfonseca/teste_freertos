################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../app/GUI_ALPHA_TransparentDialogDemo.c \
../app/GUI_HelloWorld.c \
../app/GUI_HouseControlDemo.c \
../app/GUI_ReversiDemo.c \
../app/GUI_WIDGET_GraphYtDemo.c 

OBJS += \
./app/GUI_ALPHA_TransparentDialogDemo.o \
./app/GUI_HelloWorld.o \
./app/GUI_HouseControlDemo.o \
./app/GUI_ReversiDemo.o \
./app/GUI_WIDGET_GraphYtDemo.o 

C_DEPS += \
./app/GUI_ALPHA_TransparentDialogDemo.d \
./app/GUI_HelloWorld.d \
./app/GUI_HouseControlDemo.d \
./app/GUI_ReversiDemo.d \
./app/GUI_WIDGET_GraphYtDemo.d 


# Each subdirectory must supply rules for building sources it contributes
app/%.o: ../app/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DNDEBUG -D__REDLIB__ -D__USE_CMSIS -D__CODE_RED -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject_04\cmsis\inc" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject_04\freertos\include" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject_04\freertos\portable\gcc" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject_04\emwin\GUI" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject_04\emwin\Inc" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject_04\emwin\Config" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject_04\emwin\System\HW" -I"C:\Users\Ulysses\Documents\LPCXpresso_8.2.2_650\workspace\BaseProject_04\app\inc" -Os -Os -g -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


